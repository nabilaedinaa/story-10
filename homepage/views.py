from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
# from .forms import UserForm, UserProfileForm


# Create your views here.
def home(request):
    return render(request, 'home.html')

def log_in(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        user_dict = {'username':username, 'password':password}
        if user is not None:
            login(request, user)
            return redirect('/')
        
        else:
            message = {'message':'Invalid username or password'}
            return render(request, 'login.html', message)
    
    else :
        return render(request, 'login.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.profileImage = form.cleaned_data.get('image')
            user.save()
            return redirect('/login')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})

def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('/')
