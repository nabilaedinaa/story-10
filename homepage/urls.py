from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('login', views.log_in, name='log_in'),
    path('logout', views.log_out, name='log_out'),
    path('signup', views.signup, name='signup'),
   
]