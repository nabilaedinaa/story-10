from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
import time


# Create your tests here.
class UnitTest(TestCase):
    def test_home_page_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_login_page_url_exists(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)
    
    def test_signup_page_url_exists(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)
    
    def test_home_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_login_template_used(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_signup_template_used(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'signup.html')
    
    def test_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
    
    def test_login_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, log_in)
    
    def test_signup_func(self):
        found = resolve('/signup')
        self.assertEqual(found.func, signup)
    
    def test_logout_func(self):
        found = resolve('/logout')
        self.assertEqual(found.func, log_out)

class FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest,self).tearDown()
    
    def test_signup_login_logout(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("Welcome", response_page)

        # Sign up
        self.driver.find_element_by_id("signUp").click()
        time.sleep(5)

        first = 'test'
        last = 'name'
        username = 'test'
        email = 'test@test.com'
        passw = 'signup123'

        self.driver.find_element_by_id('id_first_name').send_keys(first)
        time.sleep(3)
        self.driver.find_element_by_id('id_last_name').send_keys(last)
        time.sleep(3)
        self.driver.find_element_by_id('id_username').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_id('id_email').send_keys(email)
        time.sleep(3)
        self.driver.find_element_by_id('id_password1').send_keys(passw)
        time.sleep(3)
        self.driver.find_element_by_id('id_password2').send_keys(passw)
        time.sleep(3)
        self.driver.find_element_by_id('id_image').send_keys("https://picsum.photos/id/237/200/300")
        time.sleep(3)
        self.driver.find_element_by_id('signUp').click()
        time.sleep(5)

        # Log in
        self.driver.find_element_by_name('username').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('password').send_keys(passw)
        time.sleep(3)
        self.driver.find_element_by_name('logIn').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn('test', response_page)

        #Log out
        self.driver.find_element_by_id('logOut').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn("Welcome", response_page)

        # Login with wrong username and password
        self.driver.find_element_by_id('logIn').click()
        time.sleep(3)

        self.driver.find_element_by_name('username').send_keys(passw)
        time.sleep(3)
        self.driver.find_element_by_name('password').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('logIn').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn("Invalid username or password", response_page)
    
    def test_login_but_dont_have_account(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("Welcome", response_page)

        self.driver.find_element_by_id("logIn").click()
        time.sleep(3)

        self.driver.find_element_by_id("signUp").click()
        time.sleep(3)

        response_page = self.driver.page_source
        time.sleep(3)

        self.assertIn("First name", response_page)
        self.assertIn("Last name", response_page)
        self.assertIn("Username", response_page)
        self.assertIn("Email", response_page)
        self.assertIn("Password", response_page)












    
    



